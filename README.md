Angular2 Tutorial - Tour of Heroes
==================================


Notas
-----
  - Material basado en: https://angular.io/docs/ts/latest/tutorial/
  - Fecha de preparacion: Febrero 2017

Software requerido
------------------
  - nodeJs
  - npm

Instalacion
-----------
  1. git clone path/to/this/repo.git ng2-heroes
  1. cd ng2-heroes
  1. npm install
  1. npm start
